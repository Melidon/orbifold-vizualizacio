#version 330
precision highp float;

struct Light
{
    vec3 position;
    vec3 Le;
};

struct Material
{
    vec3 ka, kd, ks;
    float shininess;
    vec3 F0;
    int rough, reflective;
};

struct Dodecahedron
{
    vec3 vertices[20];
    int planes[36];
};

struct Hit {
    float t;
    vec3 position, normal;
    int mat;
};

struct Ray {
    vec3 start, dir;
};

#define M_PI 3.1415926535897932384626433832795f
const int maxDepth = 1000;
const int maxPortals = 5;
const float epsilon = 0.0001f;
uniform vec3 wEye;
uniform vec3 La;
uniform Light light;
uniform Material materials[3]; // 0 = side, 1 = mirror, 2 = gold
uniform Dodecahedron dodecahedron;
uniform float maxRadius;
uniform vec3 paraboloidParameters;

Hit intersectParaboloid(Ray ray, Hit hit)
{
    float a = paraboloidParameters.x * ray.dir.x * ray.dir.x + paraboloidParameters.y * ray.dir.y * ray.dir.y;
    float b = 2 * paraboloidParameters.x * ray.start.x * ray.dir.x + 2 * paraboloidParameters.y * ray.start.y * ray.dir.y - paraboloidParameters.z * ray.dir.z;
    float c = paraboloidParameters.x * ray.start.x * ray.start.x + paraboloidParameters.y * ray.start.y * ray.start.y - paraboloidParameters.z * ray.start.z;

    float discr = b * b - 4.0f * a * c;
    if (discr >= 0)
    {
        float sqrt_discr = sqrt(discr);
        float t1 = (-b + sqrt_discr) / (2.0f * a);
        vec3 p = ray.start + ray.dir * t1;
        if (length(p) > maxRadius)
        {
            t1 = -1;
        }
        float t2 = (-b - sqrt_discr) / (2.0f * a);
        p = ray.start + ray.dir * t2;
        if (length(p) > maxRadius)
        {
            t2 = -1;
        }
        if (t2 > 0 && (t2 < t1 || t1 < 0))
        {
            t1 = t2;
        }
        if (t1 > 0 && (t1 < hit.t || hit.t < 0))
        {
            hit.t = t1;
            hit.position = ray.start + ray.dir * hit.t;
            vec3 dx = vec3(1, 0, 2 * paraboloidParameters.x * hit.position.x / paraboloidParameters.z);
            vec3 dy = vec3(0, 1, 2 * paraboloidParameters.y * hit.position.y / paraboloidParameters.z);
            hit.normal = normalize(cross(dx, dy));
            hit.mat = 2;
        } 
    }
    return hit;
}

void getDodecahedronPlane(int i, out vec3 p, out vec3 normal)
{
    vec3 p1 = dodecahedron.vertices[dodecahedron.planes[3 * i + 0] - 1];
    vec3 p2 = dodecahedron.vertices[dodecahedron.planes[3 * i + 1] - 1];
    vec3 p3 = dodecahedron.vertices[dodecahedron.planes[3 * i + 2] - 1];
    normal = cross(p2 - p1, p3 - p1);
    if (dot(p1, normal) < 0)
    {
        normal = -normal;
    }
    p = p1;
}

Hit intersectDodecahedron(Ray ray, Hit hit)
{
    for(int i = 0; i < 12; i++)
    {
        vec3 p1, normal;
        getDodecahedronPlane(i, p1, normal);
        float ti = abs(dot(normal, ray.dir)) > epsilon ? dot(p1 - ray.start, normal) / dot(normal, ray.dir) : -1;
        if (ti < epsilon || (ti > hit.t && hit.t > 0))
        {
            continue;
        }
        vec3 pintersect = ray.start + ray.dir * ti;
        bool outside = false;
        bool edge = false;
        for (int j = 0; j < 12; j++) 
        {
            if (i == j)
            {
                continue;
            }
            vec3 p11, n;
            getDodecahedronPlane(j, p11, n);
            float distance = dot(n, pintersect - p11);
            if (distance > 0)
            {
                outside = true;
                break;
            }
            if (distance > -0.1f)
            {
                edge = true;
            }
        }
        if (!outside)
        {
            hit.t = ti;
            hit.position = pintersect;
            hit.normal = normalize(normal);
            hit.mat = edge ? 0 : 1;
        }
    }
    return hit;
}

Hit firstIntersect(Ray ray)
{
    Hit bestHit;
    bestHit.t = -1;
    bestHit = intersectParaboloid(ray, bestHit);
    bestHit = intersectDodecahedron(ray, bestHit);
    if (dot(ray.dir, bestHit.normal) > 0)
    {
        bestHit.normal = bestHit.normal * (-1);
    }
    return bestHit;
}

vec3 Fresnel(vec3 F0, float cosTheta) { 
    return F0 + (vec3(1, 1, 1) - F0) * pow(1 - cosTheta, 5);
}

vec3 rotate(float theta, vec3 u, vec3 point)
{
    // A 3D-s forgatási mátrixot nem tudom fejből, úgyhogy megnéztem a wikipédián: https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle
    u = normalize(u);
	mat3 R;
	R[0] = vec3(cos(theta) + u.x * u.x * (1 - cos(theta)), u.x * u.y * (1 - cos(theta)) - u.z * sin(theta), u.x * u.z * (1 - cos(theta)) + u.y * sin(theta));
	R[1] = vec3(u.y * u.x * (1 - cos(theta)) + u.z * sin(theta), cos(theta) + u.y * u.y * (1 - cos(theta)), u.y * u.z * (1 - cos(theta)) - u.x * sin(theta));
	R[2] = vec3(u.z * u.x * (1 - cos(theta)) - u.y * sin(theta), u.z * u.y * (1 - cos(theta)) + u.x * sin(theta), cos(theta) + u.z * u.z * (1 - cos(theta)));
    return R * point;
}

vec3 trace(Ray ray)
{
    vec3 weight = vec3(1, 1, 1);
    vec3 outRadiance = vec3(0, 0, 0);
    for (int depth = 0, portals = 0; depth < maxDepth && portals < maxPortals; depth++)
    {
        Hit hit = firstIntersect(ray);
        if (hit.t < 0)
        {
            break;
        }
        if (materials[hit.mat].rough == 1)
        {
            vec3 lightdir = normalize(light.position - hit.position);
            float cosTheta = dot(hit.normal, lightdir);
            if (cosTheta > 0)
            {
                vec3 LeIn = light.Le / dot(light.position - hit.position, light.position - hit.position);
                outRadiance += weight * LeIn * materials[hit.mat].kd * cosTheta;
                vec3 halfway = normalize(-ray.dir + lightdir);
                float cosDelta = dot(hit.normal, halfway);
                if (cosDelta > 0)
                {
                    outRadiance += weight * light.Le * materials[hit.mat].ks * pow(cosDelta, materials[hit.mat].shininess);
                }
            }
            weight *= materials[hit.mat].ka;
            break;
        }
        if (materials[hit.mat].reflective == 1)
        {
            weight *= Fresnel(materials[hit.mat].F0, dot(-ray.dir, hit.normal));
            ray.start = hit.position + hit.normal * epsilon;
            ray.dir = reflect(ray.dir, hit.normal);
            if (hit.mat == 1)
            {
                portals++;
                ray.start = rotate(M_PI * 72.0f / 180.0f, hit.normal, ray.start);
                ray.dir = rotate(M_PI * 72.0f / 180.0f, hit.normal, ray.dir);
            }
        }
    }
    outRadiance += weight * La;
    return outRadiance;
}

in vec3 p;
out vec4 fragmentColor;

void main()
{
    Ray ray;
    ray.start = wEye;
    ray.dir = normalize(p - wEye);
    fragmentColor = vec4(trace(ray), 1);
}

//=============================================================================================
// Mintaprogram: Z�ld h�romsz�g. Ervenyes 2019. osztol.
//
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat, BOM kihuzando.
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni a printf-et kiveve
// - Mashonnan atvett programresszleteket forrasmegjeloles nelkul felhasznalni es
// - felesleges programsorokat a beadott programban hagyni!!!!!!!
// - felesleges kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan OpenGL fuggvenyek hasznalhatok, amelyek az oran a feladatkiadasig elhangzottak
// A keretben nem szereplo GLUT fuggvenyek tiltottak.
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : Szommer Zsombor
// Neptun : MM5NOT
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem.
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem,
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================
#include "fstream"
#include "sstream"
#include "framework.h"

struct Material
{
	vec3 ka, kd, ks;
	float shininess;
	vec3 F0;
	int rough, reflective;
};

struct RoughMaterial : Material
{
	RoughMaterial(vec3 _kd, vec3 _ks, float _shininess)
	{
		ka = _kd * M_PI;
		kd = _kd;
		ks = _ks;
		shininess = _shininess;
		rough = true;
		reflective = false;
	}
};

struct SmoothMaterial : Material
{
	SmoothMaterial(vec3 _F0)
	{
		F0 = _F0;
		rough = false;
		reflective = true;
	}
};

struct Camera
{
	vec3 eye, lookat, right, pvup, rvup;
	float fov = 60 * (float)M_PI / 180;

	Camera() : eye(1, 0, 1), pvup(0, 0, 1), lookat(0, 0, 0) { set(); }
	void set()
	{
		vec3 w = eye - lookat;
		float f = length(w);
		right = normalize(cross(pvup, w)) * f * tanf(fov / 2);
		rvup = normalize(cross(w, right)) * f * tanf(fov / 2);
	}
	void Animate(float t)
	{
		float x = eye.x - lookat.x;
		float y = eye.y - lookat.y;
		float r = sqrt(x * x + y * y);
		eye = vec3(r * cos(t) + lookat.x, r * sin(t) + lookat.y, eye.z);
		set();
	}
};

GPUProgram shader;
Camera camera;
bool animate = true;

void createShader()
{
	std::fstream vertexStream("vertex.glsl");
	std::fstream fragmentStream("fragment.glsl");
	std::stringstream vertex;
	std::stringstream fragment;
	vertex << vertexStream.rdbuf();
	fragment << fragmentStream.rdbuf();
	vertexStream.close();
	fragmentStream.close();

	shader.create(vertex.str().c_str(), fragment.str().c_str(), "outColor");
}

void setLight()
{
	shader.setUniform(vec3(0.4f, 0.4f, 0.25f), "light.position");
	shader.setUniform(vec3(0.5f, 0.6f, 0.6f), "La");
	shader.setUniform(vec3(0.8f, 0.8f, 0.8f), "light.Le");
}

float F(float n, float k)
{
	return ((n - 1) * (n - 1) + k * k) / ((n + 1) * (n + 1) + k * k);
}

void setMaterials()
{
	std::vector<Material> materials;

	Material edge = RoughMaterial(vec3(0.1f, 0.2f, 0.3f), vec3(5, 5, 5), 500);
	Material mirror = SmoothMaterial(vec3(1, 1, 1));
	Material gold = SmoothMaterial(vec3(F(0.17, 3.1), F(0.35, 2.7), F(1.5, 1.9)));

	materials.push_back(edge);
	materials.push_back(mirror);
	materials.push_back(gold);

	char name[256];
	for (unsigned int mat = 0; mat < materials.size(); mat++)
	{
		sprintf(name, "materials[%d].ka", mat);
		shader.setUniform(materials[mat].ka, name);
		sprintf(name, "materials[%d].kd", mat);
		shader.setUniform(materials[mat].kd, name);
		sprintf(name, "materials[%d].ks", mat);
		shader.setUniform(materials[mat].ks, name);
		sprintf(name, "materials[%d].shininess", mat);
		shader.setUniform(materials[mat].shininess, name);
		sprintf(name, "materials[%d].F0", mat);
		shader.setUniform(materials[mat].F0, name);
		sprintf(name, "materials[%d].rough", mat);
		shader.setUniform(materials[mat].rough, name);
		sprintf(name, "materials[%d].reflective", mat);
		shader.setUniform(materials[mat].reflective, name);
	}
}

void setDodecahedron()
{
	const float g = 0.618f, G = 1.618f;
	std::vector<vec3> v = {
		vec3(0, g, G), vec3(0, -g, G), vec3(0, -g, -G), vec3(0, g, -G),
		vec3(G, 0, g), vec3(-G, 0, g), vec3(-G, 0, -g), vec3(G, 0, -g),
		vec3(g, G, 0), vec3(-g, G, 0), vec3(-g, -G, 0), vec3(g, -G, 0),
		vec3(1, 1, 1), vec3(-1, 1, 1), vec3(-1, -1, 1), vec3(1, -1, 1),
		vec3(1, -1, -1), vec3(1, 1, -1), vec3(-1, 1, -1), vec3(-1, -1, -1)};
	for (int i = 0; i < v.size(); i++)
	{
		shader.setUniform(v[i], "dodecahedron.vertices[" + std::to_string(i) + "]");
	}
	std::vector<int> planes = {
		1, 2, 16,
		1, 13, 9,
		1, 14, 6,
		2, 15, 11,
		3, 4, 18,
		3, 17, 12,
		3, 20, 7,
		19, 10, 9,
		16, 12, 17,
		5, 8, 18,
		14, 10, 19,
		6, 7, 20};
	for (int i = 0; i < planes.size(); i++)
	{
		shader.setUniform(planes[i], "dodecahedron.planes[" + std::to_string(i) + "]");
	}
}

void setParaboloid()
{
	shader.setUniform(0.3f, "maxRadius");
	shader.setUniform(vec3(1.25f, 2.5f, 0.5f), "paraboloidParameters");
}

void onInitialization()
{
	glViewport(0, 0, windowWidth, windowHeight);

	unsigned int vao, vbo;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	float vertexCoords[] = {-1, -1, 1, -1, 1, 1, -1, 1};
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexCoords), vertexCoords, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	createShader();

	setLight();
	setMaterials();
	setDodecahedron();
	setParaboloid();
}

void onDisplay()
{
	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	shader.setUniform(camera.eye, "wEye");
	shader.setUniform(camera.lookat, "wLookAt");
	shader.setUniform(camera.right, "wRight");
	shader.setUniform(camera.rvup, "wUp");
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glutSwapBuffers();
}

void onKeyboard(unsigned char key, int pX, int pY)
{
	if (key == 'a')
	{
		animate = !animate;
	}
}

void onKeyboardUp(unsigned char key, int pX, int pY) {}
void onMouse(int button, int state, int pX, int pY) {}
void onMouseMotion(int pX, int pY) {}
void onIdle()
{
	if (animate)
	{
		camera.Animate(glutGet(GLUT_ELAPSED_TIME) / 4000.0f);
	}
	glutPostRedisplay();
}
